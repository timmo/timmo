import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import BrushIcon from '@material-ui/icons/Brush';
import LogoIcon from '../resources/logo-icon.svg';
import GithubCircle from 'mdi-material-ui/GithubCircle';
import StackOverflow from 'mdi-material-ui/StackOverflow';
import Twitter from 'mdi-material-ui/Twitter';
import Twitch from 'mdi-material-ui/Twitch';
import GooglePlay from 'mdi-material-ui/GooglePlay';
import HomeAssistant from 'mdi-material-ui/HomeAssistant';
import Youtube from 'mdi-material-ui/Youtube';

const styles = theme => ({
  root: {
    position: 'absolute',
    minHeight: '100%',
    minWidth: '100%',
    background: theme.palette.backgrounds.main
  },
  buttons: {
    position: 'fixed',
    top: 0,
    right: 0,
    display: 'block'
  },
  card: {
    margin: '3.0rem 24.0rem',
    [theme.breakpoints.down('lg')]: {
      margin: '3.0rem 12.0rem'
    },
    [theme.breakpoints.down('md')]: {
      margin: '3.0rem 8.0rem'
    },
    [theme.breakpoints.down('sm')]: {
      margin: '3.0rem 2.0rem'
    }
  },
  media: {
    backgroundSize: 'contain',
    marginTop: '1.0rem',
    height: 300,
    [theme.breakpoints.down('lg')]: {
      height: 200
    },
    [theme.breakpoints.down('md')]: {
      height: 150
    },
    [theme.breakpoints.down('sm')]: {
      height: 100
    }
  },
  title: {
    color: theme.palette.text.main,
    fontSize: '3.0rem'
  },
  grid: {
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2
  },
  iconButton: {
    height: '8.0rem',
    width: '8.0rem'
  },
  icon: {
    fontSize: '6.0rem'
  }
});

class Root extends Component {
  state = {
    anchorEl: null,
  };

  componentDidMount = () => this.setTheme();

  setTheme = (themeId = undefined) => {
    if (!themeId && themeId !== 0)
      themeId = Number(localStorage.getItem('theme'));
    if (!themeId && themeId !== 0)
      themeId = 0;
    this.props.setTheme(themeId);
    localStorage.setItem('theme', themeId);
  };

  handleClick = event => this.setState({ anchorEl: event.currentTarget });

  handleClose = (value) => this.setState({ anchorEl: null }, () => {
    if (Number(value))
      this.setTheme(value);
  });

  render() {
    const { classes, theme, themes } = this.props;
    const { anchorEl } = this.state;

    return (
      <div className={classes.root}>
        <div className={classes.buttons}>
          <IconButton
            className={classes.button}
            aria-label="Theme"
            aria-owns={anchorEl ? 'simple-menu' : null}
            aria-haspopup="true"
            onClick={this.handleClick}>
            <BrushIcon />
          </IconButton>
          <Menu
            id="theme"
            value={theme}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={this.handleClose}>
            {themes.map(theme => {
              return (
                <MenuItem key={theme.id} onClick={() => this.handleClose(theme.id)}>{theme.name}</MenuItem>
              );
            })}
          </Menu>
        </div>

        <Card className={classes.card}>
          <CardMedia
            className={classes.media}
            image={LogoIcon}
            title="Logo" />
          <CardContent>

            <Grid
              className={classes.grid}
              container
              justify="center">
              <Grid item>
                <Typography variant="headline" component="h1" className={classes.title}>
                  Timmo
              </Typography>
              </Grid>
            </Grid>
            <Grid
              className={classes.grid}
              container
              justify="space-around">

              <Grid item>
                <IconButton className={classes.iconButton} href="https://github.com/timmo001" target="_blank">
                  <GithubCircle className={classes.icon} />
                </IconButton>
              </Grid>
              <Grid item>
                <IconButton className={classes.iconButton} href="https://stackoverflow.com/users/1888770/timmo" target="_blank">
                  <StackOverflow className={classes.icon} />
                </IconButton>
              </Grid>
              <Grid item>
                <IconButton className={classes.iconButton} href="https://twitter.com/timmo001" target="_blank">
                  <Twitter className={classes.icon} />
                </IconButton>
              </Grid>
              <Grid item>
                <IconButton className={classes.iconButton} href="https://www.twitch.tv/timmo001" target="_blank">
                  <Twitch className={classes.icon} />
                </IconButton>
              </Grid>
              <Grid item>
                <IconButton className={classes.iconButton} href="https://www.youtube.com/user/TIMMO54321/" target="_blank">
                  <Youtube className={classes.icon} />
                </IconButton>
              </Grid>
              <Grid item>
                <IconButton className={classes.iconButton} href="https://play.google.com/store/apps/dev?id=5292588541115872750" target="_blank">
                  <GooglePlay className={classes.icon} />
                </IconButton>
              </Grid>
              <Grid item>
                <IconButton className={classes.iconButton} href="https://community.home-assistant.io/u/timmo001/" target="_blank">
                  <HomeAssistant className={classes.icon} />
                </IconButton>
              </Grid>
              
            </Grid>

          </CardContent>
        </Card>
      </div>
    );
  }
}

Root.propTypes = {
  classes: PropTypes.object.isRequired,
  themes: PropTypes.array.isRequired,
  theme: PropTypes.object.isRequired,
  addTheme: PropTypes.func.isRequired,
  setTheme: PropTypes.func.isRequired,
};

export default withStyles(styles)(Root);
